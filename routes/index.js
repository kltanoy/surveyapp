var express = require('express');
var router = express.Router();
var mysql = require('mysql');

var connection = mysql.createConnection({
  host: 'localhost',
  port: '3307',
  user: 'root',
  password: '@v3p@$$w0rd',
  database: 'sunday_survey'
});

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Union Bank' });
});

router.get('/thankyou', function (req, res, next) {
  res.render('thankyou');
});

router.post('/regcatch', function (req, res) {
  var data = {
    fname: req.body.fname,
    lname: req.body.lname,
    rating: req.body.star
  }

  console.log(data);

  var query = connection.query("INSERT INTO survey set ?", data, function (err, rows) {
    if (err) {
      console.log("Not Saved In DB");
      res.redirect("/error");
    } else {
      console.log("Saved in DB");
      res.render('thankyou', { rem: 'Thank you for your feedback, Have a good day!' });
    }
  });

});

module.exports = router;
